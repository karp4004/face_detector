/****************************************************************************
**
** Copyright (C) 2012 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 1.0
import "content"

Rectangle {
    id: spinner_widget
	color: "#000000"
	
	signal qmlSignal(int hour, int minute, int day)
		
    Row {
        id: sinner_row
        spacing: 10
        anchors.fill: parent

		Rectangle {
        	height: parent.height/2
        	anchors.top: parent.top
        	width: parent.width/3 - sinner_row.spacing/2
        	color: "#ffffff"
	            
	        Spinner {
	            id: hour
	            anchors.fill: parent
	            focus: true
	            model: hourModel
	            itemHeight: 30
	            delegate: Text { font.pixelSize: 25; text: model.modelData.text; height: 30 }
	        }
        }
        
		Rectangle {
        	height: parent.height/2
        	anchors.top: parent.top
        	width: parent.width/3 - sinner_row.spacing/2
        	color: "#ffffff"

	        Spinner {
	            id: minute
	            anchors.fill: parent
	            focus: true
	            model: minuteModel
	            itemHeight: 30
	            delegate: Text { font.pixelSize: 25; text: model.modelData.text; height: 30 }
	        }
		}
		
		Rectangle {
        	height: parent.height/2
        	anchors.top: parent.top
        	width: parent.width/3 - sinner_row.spacing/2
        	color: "#ffffff"
		
	        Spinner {
	            id: day
	            anchors.fill: parent
	            focus: true
	            model: dayModel
	            itemHeight: 30
	            delegate: Text { font.pixelSize: 25; text: model.modelData.text; height: 30 }
	        }
	    }
    }
         
    Button {
        id: addAlarm
        x: 0
        y: parent.height - parent.height/3
		height: parent.height/6
		width: parent.width
		text: "add alarm"
		
		MouseArea {
	     anchors.fill: parent
		     onClicked: {
		         onClicked: spinner_widget.qmlSignal(hour.currentIndex, minute.currentIndex, day.currentIndex+1)
		     }
		 }
	 }
}
