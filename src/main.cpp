/*
 * main.cpp
 *
 *  Created on: Dec 8, 2013
 *      Author: karp4004
 */
#include <QApplication>
#include <QtGui>
#include "main_widget.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWidget video(0);
    video.show();

    return app.exec();
}


