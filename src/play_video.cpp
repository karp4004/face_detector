/*
 * play_video.cpp
 *
 *  Created on: Jan 21, 2014
 *      Author: karp4004
 */
#include "opencv2/highgui/highgui.hpp"
#include "stdio.h"

int main(int argc, char** argv) {

	fprintf(stderr, "1\n");
	/* Create a window */
    cvNamedWindow("Example2", CV_WINDOW_AUTOSIZE);
    /* capture frame from video file */
    CvCapture* capture = cvCreateFileCapture( argv[1]);fprintf(stderr, "1\n");
    /* Create IplImage to point to each frame */
    IplImage* frame;fprintf(stderr, "1\n");
    /* Loop until frame ended or ESC is pressed */
    while(1) {fprintf(stderr, "1\n");
        /* grab frame image, and retrieve */
        frame = cvQueryFrame(capture);fprintf(stderr, "1\n");
        /* exit loop if fram is null / movie end */
        if(!frame) break;fprintf(stderr, "1\n");
        /* display frame into window */
        cvShowImage("Example2", frame);fprintf(stderr, "1\n");
        /* if ESC is pressed then exit loop */
        char c = cvWaitKey(33);fprintf(stderr, "1\n");
        if(c==27) break;
    }
    /* destroy pointer to video */
    cvReleaseCapture(&capture);fprintf(stderr, "1\n");
    /* delete window */
    cvDestroyWindow("Example2");fprintf(stderr, "1\n");

    return EXIT_SUCCESS;
}



