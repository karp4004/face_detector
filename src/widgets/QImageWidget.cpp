/*
 * QImageWidget.cpp
 *
 *  Created on: Jan 20, 2014
 *      Author: OlegKarpov
 */
#include "QImageWidget.h"

#define STACK_TRACE 1
#define DEBUG_THREAD 1

QImageWidget::QImageWidget(QWidget* parent)
:QWidget(parent)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	should_repaint = false;
}

void QImageWidget::paintEvent(QPaintEvent * event)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	pthread_t self = pthread_self();
#if DEBUG_THREAD
	fprintf(stderr, "%s:%s:self:%x\n", __FILE__, __FUNCTION__, self);
#endif

//	QImage im("shot.jpg");
//	if(!im.isNull())
//	{
//		QPainter painter(this);
//		QPoint p2(0,0);
//		painter.drawImage(p2, im.scaled(size()));
//	}

	int offset = 0;
	std::list<QImage>::iterator mShotList_it = mShotList.begin();
	while(mShotList_it != mShotList.end())
	{
		fprintf(stderr, "%s:%s:offset:%d\n", __FILE__, __FUNCTION__, offset);

		if( !mShotList_it->isNull() )
		{
			QPoint p(offset,0);
			QPainter painter(this);
			painter.drawImage(p, *mShotList_it);
		}

		offset += 200;

		if(offset > width())
		{
			offset = 0;
		}

		mShotList_it++;
	}

	QWidget::paintEvent(event);
}

int QImageWidget::setImage(QImage& image)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	pthread_t self = pthread_self();
#if DEBUG_THREAD
	fprintf(stderr, "%s:%s:self:%x\n", __FILE__, __FUNCTION__, self);
#endif

	mImage = image;

	return 0;
}

int QImageWidget::addImage(QImage& image)
{
	mShotList.push_back(image);
	repaint();
	return 0;
}
