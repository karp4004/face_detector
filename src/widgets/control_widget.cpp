/*
 * main_menu.h
 *
 *  Created on: Jan 17, 2014
 *      Author: OlegKarpov
 */
#include "control_widget.h"

#include "stdio.h"

#include <QDeclarativeView>
#include <QDeclarativeContext>
#include <QGraphicsObject>
#include <QVBoxLayout>

ControlWidget::ControlWidget( std::string qmlPath, QWidget* parent)
:QWidget(parent)
{
#if CONTROLLER_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

    fprintf(stderr, "%s:%s:%d:controls_path:%s\n", __FILE__, __FUNCTION__, __LINE__, qmlPath.c_str());

	controls = new QDeclarativeView(this);
	controls->setSource(QUrl(qmlPath.c_str()));
	controls->setResizeMode(QDeclarativeView::SizeRootObjectToView);

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(controls);
	layout->setMargin(0);
	layout->setSpacing(0);
	setLayout(layout);

	setSignals(controls);
}

int ControlWidget::setAction(std::string button)
{
#if CONTROLLER_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	QGraphicsObject *root = controls->rootObject();
	QObject *play = root->findChild<QObject*>(button.c_str());
	if(play)
	{
	}
	else
	{
		fprintf(stderr, "cant find %s\n", button.c_str());
	}

	return 0;
}

int ControlWidget::setLoopAction(std::string button)
{
	QGraphicsObject *root = controls->rootObject();
	QObject *play = root->findChild<QObject*>(button.c_str());
	if(play)
	{
	}
	else
	{
		fprintf(stderr, "cant find %s\n", button.c_str());
	}

	return 0;
}

QObject* ControlWidget::findObject(std::string objectName)
{
#if CONTROLLER_STACK_TRACE
	fprintf(stderr, "%s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
#endif

	QGraphicsObject *root = controls->rootObject();
	QObject *object = root->findChild<QObject*>(objectName.c_str());
	return object;
}

QObject* ControlWidget::rootObject()
{
	return controls->rootObject();
}

int ControlWidget::setSignals(QDeclarativeView *qmlView)
{
	QGraphicsObject *root = qmlView->rootObject();

	foreach(QObject* obj, root->children())
	{
		fprintf(stderr, "obj:%s\n", obj->objectName().toStdString().c_str());
	}

	QObject *play = root->findChild<QObject*>("play_button");
	if(play)
	{
		QObject::connect(play, SIGNAL(clicked()), this, SLOT(on_play()));
	}
	else
	{
		fprintf(stderr, "cant find back_button\n");
	}

	return 0;
}

void ControlWidget::on_play()
{
	fprintf(stderr, "on_play\n");

	emit play_sig();
}
