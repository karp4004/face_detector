/*
 * main_menu.cpp
 *
 *  Created on: Jan 17, 2014
 *      Author: OlegKarpov
 */

#ifndef MAIN_MENU_H_
#define MAIN_MENU_H_

#include <QWidget>
#include <QToolButton>
#include <QComboBox>

class QLabel;
class QFrame;
class QSpinBox;
class QAbstractSlider;
class QDeclarativeView;

class ControlWidget: public QWidget
{
	Q_OBJECT

public:
	ControlWidget( std::string qmlPath, QWidget* parent=0);
	int setAction(std::string button);
	int setLoopAction(std::string button);
	QObject* findObject(std::string objectName);
	QObject* rootObject();

public slots:
	void on_play();

signals:
	void play_sig();

private:
	int setSignals(QDeclarativeView *qmlView);
    QDeclarativeView    *controls;
};

#endif /* MAIN_MENU_CPP_ */
