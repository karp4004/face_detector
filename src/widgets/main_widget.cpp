/*
 * main_widget.cpp
 *
 *  Created on: Jan 21, 2014
 *      Author: OlegKarpov
 */
#include "main_widget.h"
#include "video_widget.h"
#include "QImageWidget.h"

MainWidget::MainWidget(QWidget* parent)
:QWidget(parent)
{
    mImageWidget = new QImageWidget(this);
    mVideoWidget = new VideoWidget(this);

    QVBoxLayout *layout2 = new QVBoxLayout;
    layout2->addWidget(mImageWidget,1);
    layout2->addWidget(mVideoWidget,5);
    layout2->setMargin(0);
    layout2->setSpacing(0);

    setLayout(layout2);
    resize( 600, 400);

	connect(mVideoWidget, SIGNAL(signal_shot()), this, SLOT(on_shot()));
}

void MainWidget::on_shot()
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	fprintf(stderr, "%s:%s: have shot\n", __FILE__, __FUNCTION__);

	QImage shot("shot.jpg");
    if(!shot.isNull())
    {
    	shot = shot.scaled(200,200);
    	mImageWidget->addImage(shot);
    }
}
