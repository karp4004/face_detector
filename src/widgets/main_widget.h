/*
 * main_widget.h
 *
 *  Created on: Jan 21, 2014
 *      Author: OlegKarpov
 */

#ifndef MAIN_WIDGET_H_
#define MAIN_WIDGET_H_

#include <QtGui>

class QImageWidget;
class VideoWidget;

class MainWidget: public QWidget
{
	Q_OBJECT

public:
	MainWidget(QWidget* parent=0);

public slots:
	void on_shot();

private:
    QImageWidget* mImageWidget;
    VideoWidget* mVideoWidget;
};


#endif /* MAIN_WIDGET_H_ */
