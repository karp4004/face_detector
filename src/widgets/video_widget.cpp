/*
 * video_widget.cpp
 *
 *  Created on: Dec 8, 2013
 *      Author: karp4004
 */
#include "video_widget.h"
#include "control_widget.h"
#include <QImage>
#include "QImageWidget.h"
#include <QPushButton>
#include <QDeclarativeProperty>

#include <iostream>
#include <stdio.h>

/** Global variables */
String face_cascade_name = "schemas/lbpcascade_frontalface.xml";
String eyes_cascade_name = "schemas/haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
string window_name = "Capture - Face detection";

RNG rng(12345);

#define WIDTH 640
#define HEIGHT 480

#define VIDEO_WIDTH 1024
#define VIDEO_HEIGHT 768

#define DEBUG_PAINT 0
#define DEBUG_DETECT 1
#define DEBUG_QUEUE 0
#define DEBUG_THREAD 0
#define DEBUG_DRAG 0
#define STACK_TRACE 0
#define LOG_TIME 0

cv::Mat QImage2Mat(QImage const& src)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

     cv::Mat tmp(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine());
     cv::Mat result; // deep copy just in case (my lack of knowledge with open cv)
     cvtColor(tmp, result,CV_BGR2RGB);
     return result;
}

QImage Mat2QImage(cv::Mat const& src)
{
     cv::Mat temp(src.cols,src.rows,src.type());
     cvtColor(src, temp,CV_BGR2RGB);
     QImage dest= QImage((uchar*) src.data, src.cols, src.rows, src.step, QImage::Format_RGB888);
     return dest;
}

/**
 * @function detectAndDisplay
 */
void VideoWidget::detectAndDisplay( Mat& frame)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

   std::vector<Rect> faces;
   Mat frame_gray = frame;

   cvtColor( frame, frame_gray, COLOR_BGR2GRAY );
   equalizeHist( frame_gray, frame_gray );

   //-- Detect faces
   face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0, Size(80, 80) );

#if DEBUG_DETECT
   fprintf(stderr ,"%s:%s:faces.size():%d\n", __FILE__, __FUNCTION__, faces.size());
#endif

   for( size_t i = 0; i < faces.size(); i++ )
    {
	    Mat faceROI = frame_gray( faces[i] );
        std::vector<Rect> eyes;


        //-- In each face, detect eyes
      eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, Size(30, 30) );

#if DEBUG_DETECT
      fprintf(stderr ,"%s:%s:eyes.size():%d\n", __FILE__, __FUNCTION__, eyes.size());
#endif

      if( eyes.size() > 1)
      {
         //-- Draw the face
         Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
         ellipse( frame, center, Size( faces[i].width/2, faces[i].height/2), 0, 0, 360, Scalar( 255, 0, 0 ), 2, 8, 0 );

         for( size_t j = 0; j < eyes.size(); j++ )
          { //-- Draw the eyes
            Point eye_center( faces[i].x + eyes[j].x + eyes[j].width/2, faces[i].y + eyes[j].y + eyes[j].height/2 );
            int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
            circle( frame, eye_center, radius, Scalar( 255, 0, 255 ), 3, 8, 0 );
          }

//         Mat gray_image;
//         cvtColor( image, gray_image, CV_Luv2RGB );

         char fname[1024];
         time_t t = time(0);
         sprintf(fname, "shot.jpg",t);
         imwrite( fname, frame );

//         mShotMutex.lock();
//         shotImage = frame;
//         mShotMutex.unlock();

         emit signal_shot();
       }


    }
   //-- Show what you got
   //imshow( window_name, frame );
}

void *frame_thread(void *param)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	pthread_t thread = pthread_self();

	while (1) {
		struct VideoWidget *w = (struct VideoWidget *)param;

		Mat frame;
		if(!w->pop_frame(&frame))
		{
			w->detectAndDisplay(frame);
		}
	}

	return 0;
}

int VideoWidget::push_frame(Mat* cvFrame)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	mMutex.lock();
#if DEBUG_QUEUE
	fprintf(stderr, "%s:%s:count:%d\n", __FILE__, __FUNCTION__, mFrameQueue.size());
#endif

	mFrameQueue.push_back(*cvFrame);
	mCond.wakeAll();
	mMutex.unlock();

	return 0;
}

int VideoWidget::pop_frame(Mat* frame)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	int res = -1;
	mMutex.lock();

	mCond.wait ( &mMutex);
	if(mFrameQueue.size()>0)
	{
		*frame = mFrameQueue.front();
		int c=mFrameQueue.size()/25 + 1;
		for(int i=0;i<c;i++)
		{
			mFrameQueue.pop_front();
		}

	#if DEBUG_QUEUE
		fprintf(stderr, "%s:%s:count:%d:erase:%d\n", __FILE__, __FUNCTION__, mFrameQueue.size(), c);
	#endif

		res = 0;
	}

	mMutex.unlock();

	return res;
}

int VideoWidget::process_detect(QImage& frame)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	pthread_t self = pthread_self();
#if DEBUG_THREAD
	fprintf(stderr, "%s:%s:self:%x\n", __FILE__, __FUNCTION__, self);
#endif

	Mat cvFrame = QImage2Mat(frame);//(VIDEO_WIDTH, VIDEO_HEIGHT, CV_8UC3, frame.bits());
	detectAndDisplay( cvFrame);

	return 0;
}

VideoWidget::VideoWidget(QWidget* parent)
:mTimer(string("timer"))
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

    mFps = 0;

	//-- 1. Load the cascade
	if( !face_cascade.load( face_cascade_name ) ){ printf("--(!)Error loading\n"); return; };
	if( !eyes_cascade.load( eyes_cascade_name ) ){ printf("--(!)Error loading\n"); return; };

    bottom_controls = new ControlWidget("qml/control_widget/bottom_controls.qml", this);
    bottom_controls->setStyleSheet("background-color:rgba(0, 0, 0, 0);");

	mTimeProgress = bottom_controls->findObject("time_progress");
	if(!mTimeProgress)
	{
		fprintf(stderr, "cant find time_progress\n");
	}
	else
	{
		connect( mTimeProgress, SIGNAL(progressDragged( qreal )),
				 this, SLOT(progressDragged( qreal )) );
	}

    QWidget* dummy = new QWidget(this);

    QFont f;
    f.setPointSize(20);

    mLabel = new QLabel(this);
    mLabel->setText("none");
    mLabel->setFont(f);

    QPalette palette = mLabel->palette();
    palette.setColor(mLabel->backgroundRole(), Qt::white);
    palette.setColor(mLabel->foregroundRole(), Qt::white);
    mLabel->setPalette(palette);

    QVBoxLayout *layout2 = new QVBoxLayout;
    layout2->addWidget(mLabel,1);
    layout2->addWidget(dummy,3);
    layout2->addWidget(bottom_controls,1);
    layout2->setMargin(0);
    layout2->setSpacing(0);

    setLayout(layout2);
    resize( 600, 400);

    connect( this, SIGNAL( frameReady() ),
             this, SLOT( processNewFrame() ) );

    connect( bottom_controls, SIGNAL( play_sig() ),
    		this, SLOT( on_play() ) );

	pthread_mutex_init(&frameMutex, NULL);
	pthread_cond_init(&frameCond, NULL);

    pthread_t th;
	if (pthread_create(&th, 0, frame_thread, this) != 0) {
		fprintf(stderr, "ERROR: can`t start main thread\n");
	}

	QTimer *timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(on_timeout()));
	timer->start(25);
}

void VideoWidget::on_timeout()
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	if( capture.isOpened() )
	{
		Mat frame;
		capture >> frame;

		if( !frame.empty() )
		{
			push_frame(&frame);
			currnetFrame = Mat2QImage(frame).scaled(size());
			currnetFrame = currnetFrame.rgbSwapped();
			repaint();
		}
	}
}

void VideoWidget::on_play()
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	std::string cp = QDir::currentPath().toStdString();
	cp += "/videos/faces1.avi";
    openFile("rtsp://127.0.0.1:5540/stream.cgi?devid=83&devtp=1&prtid=o1&strtp=mjpg");//cp.c_str());
}

VideoWidget::~VideoWidget()
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

}

void VideoWidget::setProgressPosition(float pos)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	if(mTimeProgress)
	{
		if(pos != -1.)
		{
			bool dragging = QDeclarativeProperty::read(mTimeProgress, "dragging").toBool();

#if DEBUG_DRAG
			fprintf(stderr, "%s:%s:%d:dragging:%d\n", __FILE__, __FUNCTION__, __LINE__, dragging);
#endif

			if(!dragging)
			{
				qreal currentPosition = pos;
				QDeclarativeProperty::write(mTimeProgress, "currentPosition", currentPosition);
			}
		}
	}
}

void VideoWidget::progressDragged(qreal pos)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	float new_pos = pos;

#if DEBUG_DRAG
	fprintf(stderr, "%s:%s:%d:new position:%f\n", __FILE__, __FUNCTION__, __LINE__, new_pos);
#endif

}

void VideoWidget::paintEvent(QPaintEvent * event)
{
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	pthread_t self = pthread_self();
#if DEBUG_THREAD
	fprintf(stderr, "%s:%s:self:%x\n", __FILE__, __FUNCTION__, self);
#endif

	mTimer.Update();

#if LOG_TIME
	mTimer.Log();
#endif

    mFps++;

    if(mTimer.GetElapsed() > 1.0)
    {
		char msecs_str[64];
		sprintf(msecs_str, "FPS:%f", mFps);
		mLabel->setText(msecs_str);

		mFps = 0;
		mTimer.Reset();
    }

	QPainter painter(this);


	float pos = 0;

#if DEBUG_PAINT
	fprintf(stderr, "%s:%s:time:%d-%d-%f\n", __FILE__, __FUNCTION__, ct, l, pos);
#endif

	setProgressPosition(pos);

	if( !currnetFrame.isNull() )
	{
		QPoint p(0,0);
		painter.drawImage(p, currnetFrame);//.scaled(size()));
	}

//	int offset = 0;
//	std::list<QImage>::iterator mShotList_it = mShotList.begin();
//	while(mShotList_it != mShotList.end())
//	{
//		fprintf(stderr, "%s:%s:offset:%d\n", __FILE__, __FUNCTION__, offset);
//
//		if( !mShotList_it->isNull() )
//		{
//			QPoint p(offset,0);
//			painter.drawImage(p, *mShotList_it);
//		}
//
//		offset += 200;
//		mShotList_it++;
//	}

	QWidget::paintEvent( event);

	//bottom_controls->repaint();
}

void VideoWidget::openFile(QString file) {
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

	  capture.open( file.toStdString().c_str());
}

void VideoWidget::stop() {
#if STACK_TRACE
	fprintf(stderr, "%s:%s\n", __FILE__, __FUNCTION__);
#endif

}
