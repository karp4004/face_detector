/*
 * video_widget.h
 *
 *  Created on: Dec 8, 2013
 *      Author: karp4004
 */

#ifndef VIDEO_WIDGET_H_
#define VIDEO_WIDGET_H_

#include <QtGui>
#include <QTime>
#include <QMutex>
#include <QWaitCondition>
#include "PosixTimer.h"
#include <list>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

class ControlWidget;
class QImage;
class QMutex;
class QImageWidget;


class VideoWidget: public QWidget
{
	Q_OBJECT
public:
	VideoWidget(QWidget* parent);
	~VideoWidget();
	int takeFrame();

	void openFile(QString file);
	void stop();

    int push_frame(Mat* cvFrame);
    int pop_frame(Mat* cvFrame);
    int process_detect(QImage& frame);
    void detectAndDisplay( Mat& frame);

public slots:
	void on_play();
	void progressDragged(qreal pos);
	void on_timeout();

signals:
	void signal_shot();


protected:
	void paintEvent(QPaintEvent * event);

private:
	void setProgressPosition(float pos);

	ControlWidget* bottom_controls;
    ControlWidget* controls;
    QObject *mTimeProgress;
    QLabel* mLabel;

    float mFps;
    PosixTimer mTimer;

    QImage currnetFrame;

    std::list<Mat> mFrameQueue;
    pthread_mutex_t frameMutex;
    pthread_cond_t frameCond;

    VideoCapture capture;
    QMutex mMutex;

    QWaitCondition mCond;

    QMutex mShotMutex;
	Mat shotImage;
	QImage shotQImage;

	std::list<QImage> mShotList;
};

#endif /* VIDEO_WIDGET_H_ */
